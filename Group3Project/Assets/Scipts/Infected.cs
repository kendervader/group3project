﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Infected : MonoBehaviour
{
    public static int infectedCount;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void infect()
    {
        //called when an area is infected
        infectedCount += 1;
        print(infectedCount.ToString());
    }
}
