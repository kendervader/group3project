﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    public Transform player;

    public Camera playerCamera;
    
    public float mouseSpeed = 1;
    
    void Update()
    {
        float X = Input.GetAxis("Mouse X") * mouseSpeed;
        float Y = Input.GetAxis("Mouse Y") * mouseSpeed;
        
        player.Rotate(0, X, 0);
        
        //var y = Mathf.Clamp(-Y, 10, 5);
        //playerCamera.transform.eulerAngles.x = Mathf.Clamp(playerCamera.transform.eulerAngles.x, 0, 80);
        //var rotX = playerCamera.transform.position.x;
        //var rotY = playerCamera.transform.position.y;
        //var rotZ = playerCamera.transform.position.z;
        
        
        //playerCamera.transform.position = new Vector3(rotX, rotY, rotZ);
        
        if (playerCamera.transform.eulerAngles.x + (-Y) > 80 && playerCamera.transform.eulerAngles.x + (-Y) < 280)
        {
            
        }
        else
        {
            playerCamera.transform.RotateAround(player.position, playerCamera.transform.right, -Y);
        }
    }
}
