﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;

public class CountdownTimer : MonoBehaviour
{
    public float timeLeft = 3.0f;
     
    public TextMeshProUGUI countdownText;
    
    public GameObject gameOver0;

    void Update()
    {
        timeLeft -= Time.deltaTime;
        countdownText.text = "Time Left:" + Mathf.Round(timeLeft);
        if(timeLeft < 0)
        {
            gameOver0.SetActive(true);
            RestartLevel();
        }
    }
    
    IEnumerator RestartLevel()
    {
        yield return new WaitForSeconds(5);
        
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
       // SceneManager.LoadScene("DiseaseDash");
    }
}
