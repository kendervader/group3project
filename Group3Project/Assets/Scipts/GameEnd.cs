﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameEnd : MonoBehaviour
{
    public Slider hungerSlider;

    public GameObject gameOver1;
    public GameObject gameOver2;
    public GameObject gameOver3;
    public GameObject gameOver4;
    public GameObject gameOver5;

    //public GameObject infectionCounter;
    
    
    
    void Update()
    {
        if (hungerSlider.value == hungerSlider.maxValue)
        {
            Debug.Log("Game Over");
            if (Infected.infectedCount == 0)
            {
                gameOver1.SetActive(true);
                RestartLevel();
            }
            if (Infected.infectedCount == 1)
            {
                gameOver2.SetActive(true);
                RestartLevel();
            } 
            if (Infected.infectedCount == 2)
            {
                gameOver3.SetActive(true);
                RestartLevel();
            } 
            if (Infected.infectedCount == 3)
            {
                gameOver4.SetActive(true);
                RestartLevel();
            }
            if (Infected.infectedCount == 4)
            {
                gameOver5.SetActive(true);
                RestartLevel();
            }
        }
    }
    
    IEnumerator RestartLevel()
    {
        yield return new WaitForSeconds(5);

        SceneManager.LoadScene("DiseaseDash");
    }
    
}
