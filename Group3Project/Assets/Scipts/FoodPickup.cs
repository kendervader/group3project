﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FoodPickup : MonoBehaviour
{
    public Slider foodSlider;

    public int foodValue = 1;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foodSlider.value += foodValue;
            
            Destroy(gameObject);
        }
    }
}
