﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LocationInfection : MonoBehaviour
{
    //time until room is infected
    public float infectionTime = 5;
    private GameObject infectionCounter;
    private Infected infected;
    public GameObject particles;

    private void Start()
    {
        //finds the object that has the infected script and assigns it
        infectionCounter = GameObject.Find("infectionCounter");
        infected = infectionCounter.GetComponent<Infected>();
    }

    private void Update()
    {
        if (infectionTime <= 0)
        {
            //if the timer runs out call infect, make particles active and destroy this
            infected.infect();
            particles.SetActive(true);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //when the player enters the area
            print("hello");
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //if the player stays inside the timer will drop
            infectionTime -= 1 * Time.deltaTime;
        }
    }

}
