﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HungerMeter : MonoBehaviour
{
    public GameObject player;
    public Slider Hunger_Meter;

    float Hunger = 0f;
    float MaxHunger = 10f;
    // Start is called before the first frame update
    void Start()
    {
        Hunger_Meter.value = CalcHunger();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Hunger += 1f;
            Hunger_Meter.value = CalcHunger();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag ("Food"))
        {
            Hunger += 1f;
            Hunger_Meter.value = CalcHunger();
            Debug.Log("Got Food");
        }
    }

    float CalcHunger()
   {
       return Hunger / MaxHunger;
   }

}
