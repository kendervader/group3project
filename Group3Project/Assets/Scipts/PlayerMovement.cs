﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController _characterController;

    public Camera playerCamera;

    public float speedMulti = 1f;
    public float jumpSpeed = 1.0F;
    public float gravity = 20.0f;

    private float _speed = 1.0f;
    
    private Vector3 _moveDirection = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        _characterController = GetComponent<CharacterController>();

        _speed = _speed * speedMulti;
    }

    // Update is called once per frame
    void Update()
    {
        if (_characterController.isGrounded)
        {
            float moveH = Input.GetAxis("Horizontal");
            float moveV = Input.GetAxis("Vertical");

            var forward = gameObject.transform.forward;
            var right = gameObject.transform.right;

            forward.y = 0f;
            right.y = 0f;
            forward.Normalize();
            right.Normalize();

            _moveDirection = -forward * moveV - right * moveH;
            
            _moveDirection *= _speed;
            
            if (Input.GetButton("Jump"))
            {
                _moveDirection.y = jumpSpeed;
            }
            
        }
        
        _moveDirection.y -= gravity * Time.deltaTime;

        _characterController.Move(_moveDirection * Time.deltaTime);
        
    }
}
